#!/usr/bin/perl
#genome_search_parse_results.pl
#version 1.0
#Reads the results from genome_search.pl and generates a summary for each match, including
#the sequence that matched
#the position of the first base (relating to direct strand)
#the position of the last base (relating to direct strand),
#the strand on which the match was found,
#the name of the gene containing the match or "not in gene",      
#the name of the nearest downstream gene,
#a description of the gene from the protFasFile,
#the distance to the nearest downstream gene,
#the total times this exact sequence was found,
#the percentage of the instances of this exact sequence that were found inside of genes,
#the average number of base pairs to the downstream gene for this exact sequence,
#the downstream gene for each of the times found and description.
#
#There are three command line options:
#-i input file
#-p prot_fas file
#-o output file
#
#The input file is the results from genome_search.pl
#The prot_fas file is the file from:
#ftp://ftp.sanger.ac.uk/pub/S_coelicolor/whole_genome/Sco.prot_fas
#It contains FASTA protein sequences with the base pairs spanned by the
#protein and the strand that encodes the protein. Some example entries are:
#
# >SCEND.02c, SCO0001, hypothetical protein 446:1123 forward MW:24748
# MTGHHESTGPGTALSSDSTCRVTQYQTAGVNARLRLFALLERRACPRARRTTWWPGRSAR
# WWSWTAWRRLLGVCCVRGRLGRRRDGGERGPGGHRGPGLATARRRSGGATELAVHCADVR
# QRERADLVRLEGFVRESVLPRAHPHTTARRRVLEVLGEAGSLCTARTVNSDEDYILCTLG
# VGHYDPDDQPPFKDGKPGWQRAGASIWNGSGAACIPHAAIEGPRK
# >SC8E7.42c, SCJ24.01c, SCEND.01c, SCO0002, hypothetical protein 1252:3813 forward MW:93489
# MGVSATGSGKTITTAACALEYFPEGRILVMVPTLDLIVQSAQSWRRVGHRAPMVAVCSVD
# KDEVLEQLGVRTTTNPIQLALWAGTGPVVVFATYASLVDREDPADVTGRRTVPGPLESAL
# AGGERLYGQRMAPFDLAILDEGHMTAGDMGRPWAAIHDNSRIPVDFRLYLTATPRILAAP
# RPQRGRDGRELVIASMEDDSSTYGTRIFDLGLAEAVERSILAGFEIDVLEIRDPDPILGL
# SEDALRGRRMALLQAALLEHAAQQNLHTTMVFHQRVEEAAAFAEQMPRTAAELYTAEASA
# AALAEAAELPASSIDAELYELEEGRHVPPDRVWADWLCGDHPIAHRRAVIQRFANGIDAQ
# NRRVHRAFLSSVRALGVGVDIAGLRGVEAVCIVGSRSSQVDVVQNIGRALRPNPDGTTKT
# ARIIIPVFLQPGEDPKDMVASASYQPLVDILQALRSHSERMVDQLASRALTRGTERRRIH
# VRPAPAAGPENGDMPEETSQTQQEVDRVTSVVVNFASPRDAADIAALTRCRVIRPQSLVW
# LEGYQALVRWRAENGITGLYAVPYDIETEVGTTKGYPLGRWVHQQRRAHRAGELDPHRKE
# LLDDAGMVWEPGDEAWENKLAAFRSYRRAHGHLAPRQDAVWGEGEAMVPIGQHLANLRRK
# GAKNGLGKDPERAAERAAQLAAIDPDWNCPWPLDWQRHYRVLADLVDADGVLPAIDPGVL
# FEGDDIGRWLTRQREASTWTQLSAEQQERLTALGVTPVERPAPAPAAPRSTKGPSKAKQS
# FQRGLAALAQWVEREGADRPVPRGHAEEIAVDGEADPVIIKLGVWVSNTRARRDKLTAEQ
# LNALRGLGMEWAA
# >SCJ30.11c, SCO0015, putative Na+/H+ antiporter 16070:17302 reverse MW:41975
# MSAPSRITAALRSDAVGGSLLIGAAVIALIWANSPLSHSYEALRSFTFGPSALHLNLSVE
# TWAADGLLAVFFFIVGNELKQELVHGELRDPRRAALPIAAALGGVAVPALVFLAFTLGSG
# GEAAGGWGIPMATGIAFAVAVLAVVGRHLPTPLRTFLLTLATVDDMSAVLVIAVACTSGI
# NFTALALAAVGLAVFGYLQNGSGRAVARVRAMVPAWLLFVPLAAVVWALMHACGVHATIA
# GVVMGLLMRTRPQGAERVSPSHRAEEVLRPFSAGIALPLFALMSAGVSLAGAGGFVTSAI
# TWNVLAGLLVGKVVGIFGGTWPTSRLTSAHLNPLLGWADIAGIAVLGGIGFTVSLPIAEL
# SSTSQAHLTDAKGAILLASTTAALLAALLLGRRSRHHQRLARQAAQQATT
# >SCJ30.12, SCO0016, hypothetical protein 17715:17918 forward MW:7193
# VTTPSDDTARPVPAVLPAGPVPGHPQQEAAARLWRRRQTHTGRVYLLGPPSHRGIEDGGV
# EAAEHRV
#
#
#
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk.

use warnings;
use strict;

#Command line processing.
use Getopt::Long;

my $inputFile;
my $protFasFile;
my $outputFile;
my $sortMethod;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'p|prot_fas_file=s' => \$protFasFile,
	    'o|output_file=s' => \$outputFile);

if(!defined($inputFile)) {
    die ("Usage: genome_search.pl -i <input file> -p <prot_fas file> -o <output file>\n");
}

if(!defined($protFasFile)) {
    die ("Usage: genome_search.pl -i <input file> -p <prot_fas file> -o <output file>\n");    
}

if(!defined($outputFile)) {
    die ("Usage: genome_search.pl -i <input file> -p <prot_fas file> -o <output file>\n");    
}


my @input;
#Input will be placed in an array of hashes containing the following fields:
#	       sequence     => "agagaagagag",  the sequence that matched
#	       first_base   => "123",            the position of the first base (relating to direct strand)
#	       last_base    => "232",            the position of the last base (relating to direct strand)
#	       strand       => "direct",       the strand on which the match was found

my @protFas;
#Contents of the protFasFile will be placed in an array of hashes containing the following fields:
#              gene_name    => "SCJ30.11c",
#              gene_info    => "SCO0015, putative Na+/H+ antiporter"
#              first_base   => "16070",
#              last_base    => "17302",
#              strand       => "direct"

my @output;
#Output will be placed in an array of hashes containing the following fields:
#
#	       sequence     => "agagaagagag",  the sequence that matched
#	       first_base   => "123",            the position of the first base (relating to direct strand)
#	       last_base    => "232",            the position of the last base (relating to direct strand)
#	       strand       => "direct",       the strand on which the match was found
#	       in_gene      => "SCj30.11d",    the name of the gene containing the match or "not in gene"      
#	       downstream_gene => "SCJ30.12",  the name of the nearest downstream gene
#              downstream_gene_description => "SCO0015, putative Na+/H+ antiporter" additional information from the protFasFile
#	       downstream_distance => "567",   the distance to the nearest downstream gene
#	       total_times_found => "12",      the total times this exact sequence was found
#	       percent_in_gene => "43",        the percentage of this exact sequence that were found inside of genes
#              average_distance_to_downstream => "87" the average base pairs to the downstream gene for this exact sequence
#              downstream_list => "sco23(description) sco34c.1(description)" the downstream gene for each of the times found and description.	       

#Now open the file specified by $inputFile and fill the @input array.
open (INFILE, $inputFile) or die( "Cannot open file for input: $!" );
while (<INFILE>) {
    #example of line is:
    #gtggggcgagggcgaggcgatggt	3171	3194	direct    
    if ($_ =~ m/^(\S+)\t(\d+)\t(\d+)\t(direct|reverse)/) {
	my %tempHash = ( sequence => "",
			 first_base => "0",
			 last_base => "0",
			 strand => "" );

	$tempHash {'sequence'} = $1;
	$tempHash {'first_base'} = $2;
	$tempHash {'last_base'} = $3;
	$tempHash {'strand'} = $4;
	push @input, {%tempHash};
    }
}
close (INFILE) or die( "Cannot close file for input: $!");

#Now open the protFas file specified by $protFasFile and fill the @protFas array.
open (PROTFAS, $protFasFile) or die( "Cannot open file for input: $!" );
while (<PROTFAS>) {
    #See format example at top.  
    if ($_ =~ m/^>([^,]+),([\s\S]*?)(\d+):(\d+)\s(forward|reverse)/) {
	my %tempHash = ( gene_name => "",
			 gene_info => "",
			 first_base => "0",
			 last_base => "0",
			 strand => "" );

	$tempHash {'gene_name'} = $1;
	$tempHash {'gene_info'} = $2;
	$tempHash {'first_base'} = $3;
	$tempHash {'last_base'} = $4;
	$tempHash {'strand'} = $5;
	if ($5 eq 'forward') {
	    $tempHash {'strand'} = 'direct';
	}
	else {
	    $tempHash {'strand'} = 'reverse';
	}
	push @protFas, {%tempHash};
    }
}
close (PROTFAS) or die( "Cannot close file for input: $!");

#Now construct the @output array and fill in the first three hash values.
foreach (@input) {
    my %tempHash = (sequence => "",  
	       first_base   => "",    
               last_base    => "",         
	       strand       => "",           
	       in_gene      => "",          
	       downstream_gene => "",  
	       downstream_gene_description => "",
	       downstream_distance => "",   
	       total_times_found => "",     
	       percent_in_gene => "",        
               average_distance_to_downstream => "", 
	       downstream_list => "");
    $tempHash {'sequence'} = $_->{'sequence'};
    $tempHash {'first_base'} = $_->{'first_base'};
    $tempHash {'last_base'} = $_->{'last_base'};
    $tempHash {'strand'} = $_->{'strand'};
    push @output, {%tempHash};
}

#for debugging make a shorter version of @output
#@output = splice (@output, 0, 20);

my $inputRecords = scalar(@input);
my $protFasRecords = scalar(@protFas);

#Write some of the information to $outputFile.
open(OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");
my $now = gmtime;
print(OUTFILE "#-------------------------------------------------------------------------------------------\n");
print(OUTFILE "#Results produced by genome_search_parse_results.pl search of $inputFile performed at $now (GM time).\n");
print(OUTFILE "#$inputRecords matches were analyzed. $protFasRecords genes were read from $protFasFile.\n");
print(OUTFILE "#Each record in the output contains the following information:\n");
print(OUTFILE "#sequence - the sequence that matched.\n");
print(OUTFILE "#first base - the position of the first base (relating to the direct strand).\n");
print(OUTFILE "#last base - the position of the last base.\n");
print(OUTFILE "#strand - the strand on which the match was found.\n");
print(OUTFILE "#in gene - if the match is inside of a gene, the name of the gene.\n");
print(OUTFILE "#downstream gene - the name of the nearest downstream gene.\n");
print(OUTFILE "#downstream gene description - more information describing the downstream gene.\n");
print(OUTFILE "#downstream distance - the distance to the nearest downstream gene.\n");
print(OUTFILE "#total times found - the total times this exact sequence was found.\n");
print(OUTFILE "#percentage in gene - the percentage of instances of this exact sequence that were found inside of a gene.\n");
print(OUTFILE "#average distance to downstream - the average base pairs to the downstream gene for this exact sequence.\n");
print(OUTFILE "#downstream list - the downstream gene for each instance of this match sequence and its description.\n");
print(OUTFILE "#-------------------------------------------------------------------------------------------\n\n");
close(OUTFILE) or die ("Cannot close file for output: $!");

#Now begin filling other values in the @output array.
#For matches on the direct strand look only and genes on the direct strand.
#For matches on the reverse strand look only and genes on the reverse strand.
#Check the base ranges to see if the match is within a gene and add that gene on to in_gene
foreach (@output) {
    my $outputref = $_;
    my $pattern_first_base = $outputref->{'first_base'};
    my $pattern_last_base = $outputref->{'last_base'};
    my $pattern_strand = $outputref->{'strand'};
    my $closest_downstream_gene = "no downstream gene";
    my $downstream_description = "no downstream gene";
    my $downstream_gene_distance = "";
    foreach (@protFas) {
	my $gene_name = $_->{'gene_name'};
	my $gene_description = $_->{'gene_info'};
	my $gene_first_base = $_->{'first_base'};
	my $gene_last_base = $_->{'last_base'};
	my $gene_strand = $_->{'strand'};
	#skip if not on the same strand
	if ($pattern_strand ne $gene_strand) {	    
	    next;
	}

	#In the following section determine if the pattern overlaps with the gene. The dashes
	#are used to show the type of overlap being detected.
	#         -----------  this is the pattern
	#             ----------------------------------  this is the gene
	if (($pattern_first_base <= $gene_first_base) && ($pattern_last_base >= $gene_first_base)) {
	    $outputref ->{'in_gene'} = $outputref ->{'in_gene'} . $gene_name . " ";	    
	}
	#                                        ----------- this is the pattern
	#             ----------------------------------  this is the gene
	elsif (($pattern_first_base <= $gene_last_base) && ($pattern_last_base >= $gene_last_base)) {
	    $outputref ->{'in_gene'} = $outputref ->{'in_gene'} . $gene_name . " ";	    
	}
	#                       ------------  this is the pattern
	#             ---------------------------------- this is the gene
	elsif (($pattern_first_base >= $gene_first_base) && ($pattern_last_base <= $gene_last_base)) {
	    $outputref ->{'in_gene'} = $outputref ->{'in_gene'} . $gene_name . " ";
	}
	#    ----------------------------------------------  this is the pattern
	#                ---------------   this is the gene
	elsif (($pattern_first_base <= $gene_first_base) && ($pattern_last_base >= $gene_last_base)) {
	    $outputref ->{'in_gene'} = $outputref ->{'in_gene'} . $gene_name . " ";	 
	}

	#In the following section determine the closest downstream gene.
	if ($pattern_strand eq "direct") {
	    if ($pattern_last_base < $gene_first_base) {
		my $gene_distance = $gene_first_base - $pattern_last_base - 1;
		if (($downstream_gene_distance eq "") || ($gene_distance < $downstream_gene_distance)) {
		    $downstream_gene_distance = $gene_distance;
		    $downstream_description = $gene_description;
		    $closest_downstream_gene = $gene_name;
		}
	    }
	}
	elsif ($pattern_strand eq "reverse") {
	    if ($pattern_first_base > $gene_last_base) {
		my $gene_distance = $pattern_first_base - $gene_last_base - 1;
		if (($downstream_gene_distance eq "") || ($gene_distance < $downstream_gene_distance)) {
		    $downstream_gene_distance = $gene_distance;
		    $downstream_description = $gene_description;
		    $closest_downstream_gene = $gene_name;
		}		
	    }
	}
    }
    #write out the closest downstream gene and downstream distance.
    $outputref->{'downstream_distance'} = $downstream_gene_distance;
    $outputref->{'downstream_gene'} = $closest_downstream_gene;
    $outputref->{'downstream_gene_description'} = filterText($downstream_description);
}

#To calculate stats for the matches, create a hash called %stats. %stats contains hashes:
# agagagggggggggagagagag => {
#	       times_found  => "3",  the times this exact sequence is found
#              times_in_gene => "1", the number that were found inside a gene
#              times_with_downstream => "3" the number that had a downstream gene
#	       downstream_sum   => "1235", the sum of each downstream_distance value for each occurence of the exact sequence
#	       downstream_sum_not_in_gene => "232", the of each downstream_distance value but only for occurances not in a gene
#	       downstream_genes_list => "Sco1 Sco2 Sco5" a list of the downstream genes, one for each time found
# }

my %stats;
#go through each entry in @output to build %stats
foreach(@output) {
    my $outputref = $_;
    my $pattern_sequence = $outputref->{'sequence'};
    my $in_gene = $outputref->{'in_gene'};
    my $downstream_distance = $outputref->{'downstream_distance'};
    my $downstream_gene = $outputref->{'downstream_gene'};
    my $downstream_description = $outputref->{'downstream_gene_description'};

    if (defined( $stats{$pattern_sequence})) {
	my $innerHashRef = $stats{$pattern_sequence};
	$innerHashRef->{'times_found'} = $innerHashRef->{'times_found'} + 1;
	if ($in_gene ne "") {
	    #it is in a gene
	    $innerHashRef->{'times_in_gene'} = $innerHashRef->{'times_in_gene'} + 1;
	}
	
	if ($downstream_distance ne "") {	    
	    $innerHashRef->{'downstream_sum'} = $innerHashRef->{'downstream_sum'} + $downstream_distance;
	    $innerHashRef->{'times_with_downstream'} = $innerHashRef->{'times_with_downstream'} + 1;
	    if ($in_gene eq "") {
		$innerHashRef->{'downstream_sum_not_in_gene'} = $innerHashRef->{'downstream_sum_not_in_gene'} + $downstream_distance;
	    }
	}
	$innerHashRef->{'downstream_genes_list'} = $innerHashRef->{'downstream_genes_list'} . $downstream_gene . "($downstream_description) ";
    }
    else {
	my %tempHash = ( times_found => "0",
			 times_in_gene => "0",
			 times_with_downstream => "0",
			 downstream_sum => "0",
			 downstream_sum_not_in_gene => "0",
			 downstream_genes_list => "");

	$tempHash{'times_found'} = $tempHash{'times_found'} + 1;
	if ($in_gene ne "") {
	    #it is in a gene
	    $tempHash{'times_in_gene'} = $tempHash{'times_in_gene'} + 1;
	}
	
	if ($downstream_distance ne "") {	    
	    $tempHash{'downstream_sum'} = $tempHash{'downstream_sum'} + $downstream_distance;
	    $tempHash{'times_with_downstream'} = $tempHash{'times_with_downstream'} + 1;
	    if ($in_gene eq "") {
		$tempHash{'downstream_sum_not_in_gene'} = $tempHash{'downstream_sum_not_in_gene'} + $downstream_distance;
	    }
	}
	$tempHash{'downstream_genes_list'} = $tempHash{'downstream_genes_list'} . $downstream_gene . "($downstream_description) ";
	$stats{$pattern_sequence} = {%tempHash};
    }
}

#now use the %stats hash to finish filling @output
foreach(@output) {
    my $outputref = $_;
    my $pattern_sequence = $outputref->{'sequence'};
    my $in_gene = $outputref->{'in_gene'};
    my $downstream_distance = $outputref->{'downstream_distance'};
    my $statsref = $stats{$pattern_sequence};
    $outputref->{'total_times_found'} = $statsref->{'times_found'};
    $outputref->{'percent_in_gene'} = $statsref->{'times_in_gene'} / $statsref->{'times_found'} * 100;
    if ($statsref->{'times_with_downstream'} > 0) {
	$outputref->{'average_distance_to_downstream'} = $statsref->{'downstream_sum'} / $statsref->{'times_with_downstream'};
    }
    else {
	$outputref->{'average_distance_to_downstream'} = "no downstream genes";
    }
    $outputref->{'downstream_list'} = $statsref->{'downstream_genes_list'};
    
    #change in_gene entry if it is empty to say "not in gene"
    if ($in_gene eq "") {
	$outputref->{'in_gene'} = "not in gene";
    }

    #change the downstream_distance entry if it is empty to "no downstream gene"
    if ($downstream_distance eq "") {
	$outputref->{'downstream_distance'} = "no downstream gene";
    }
}

#provide hard coded sorting to sort highest number of matches to lowest
#Sort @output using a map-sort-map.
@output = map { $_->[1] }
      sort { $b->[0] <=> $a->[0]}
      map { [ getSortValue($_), $_] }
      @output;

sub getSortValue {
    my $href = shift();
    my $result = $href->{'total_times_found'};
    return $result;
}

#Client would like matches in protein coding regions to not be displayed.
#Client would like matches > 100 bases upstream of coding region to be removed.
my @tempArray;
foreach (@output) {
    my $href = $_;
    if ($href->{'downstream_distance'} eq "no downstream gene") {
	next;
    }
    if (!($href->{'in_gene'} eq "not in gene")) {
	next;
    }
    if ($href->{'downstream_distance'} > 350) {
	next;
    }
    push (@tempArray, $href);
}

my $removed = scalar(@output) - scalar(@tempArray);
@output = @tempArray;
open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");
print(OUTFILE "#Filtered out matches occuring within genes or more than 350 bases upstream of a gene.\n" . 
      "#This filtering removed $removed matches. These removed matches are included in the\n" .
      "#calculations ('total times found' for example) but are not listed as separate records below.\n" .
      "#-------------------------------------------------------------------------------------------\n\n");
close(OUTFILE) or die ("Cannot close file for output: $!");

#Write the contents of @output to $outputFile.
open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");
foreach (@output) {
    my $href = $_;
    print(OUTFILE "#sequence\n$href->{'sequence'}\n" .
                  "#first base\n$href->{'first_base'}\n" .
                  "#last base\n$href->{'last_base'}\n" .
                  "#strand\n$href->{'strand'}\n" .
                  "#in gene\n$href->{'in_gene'}\n" .
                  "#downstream gene\n$href->{'downstream_gene'}\n" .
                  "#downstream gene description\n$href->{'downstream_gene_description'}\n" .
                  "#downstream distance\n$href->{'downstream_distance'}\n" .
                  "#total times found\n$href->{'total_times_found'}\n" .
                  "#percentage in gene\n$href->{'percent_in_gene'}\n" .
                  "#average distance to downstream\n$href->{'average_distance_to_downstream'}\n" .
                  "#downstream list\n$href->{'downstream_list'}\n\n");
}
close(OUTFILE) or die ("Cannot close file for output: $!");

#Quickly write all the hash tables stored in @output.
#open(OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");
#for my $href (@output) {
#    print(OUTFILE "{ ");
#	  for my $role (keys %$href) {
#	      print(OUTFILE "$role=$href->{$role} ");
#	  }
#    print(OUTFILE "}\n");
#}
#close(OUTFILE) or die ("Cannot close file for output: $!");


sub filterText {
    my $text = shift();
    $text =~ s/^[\s,]*//;
    $text =~ s/[\s,]*$//;
    return $text;
}
