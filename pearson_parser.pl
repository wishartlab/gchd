#!/usr/bin/perl
use strict;
use Spreadsheet::ParseExcel; 
use Spreadsheet::WriteExcel;
use Statistics::Descriptive;

#Read in a Spreadsheet 
my $excel = new Spreadsheet::ParseExcel::Workbook->Parse($ARGV[0]);

#For each sheet in the excel file, do a comparison for pearson correlations
foreach my $sheet (@{$excel->{Worksheet}}) {
   
    compare_values($sheet);

}

##############################################################
# This function, given a worksheet, will try to compare
# each gene intensity against all others.
# Format restrictive
#############################################################
sub compare_values
{

    my ($sheet) = @_;
    
    my @cur_row;
    my @sub_row;
    my @ref_row;
    my @cur_ref_row;

    my $cur_row_mean;
    my $sub_row_mean;

    my $cur_row_std;
    my $sub_row_std;

    my $cur_row_stat_obj;
    my $sub_row_stat_obj;

    my $pcc;

    my $threshold = 0.6;
    my $filename = "Detail_Over.xls";
    my $threshold2 = -0.6;
    my $filename2 = "Detail_Under.xls";

    #Constants
    my $At_element_position = 0;
    my $M7Avr_element_position = 1;
    my $W7Avr_element_position = 2;
    my $M10Avr_element_position = 3;
    my $W10Avr_element_position = 4;
    my $Info_element_position = $sheet->{MaxCol};
    my $dest_row = 0;
    my $dest_col = 0;
    my $min_dest_row = 0;
    my $min_dest_col = 0;

    ################################################
    # Create new Excel File to Write
    ################################################
    my $dest_file = Spreadsheet::WriteExcel->new($filename)
	or die "Could not create a new Excel file: $!";
    my $dest_sheet = $dest_file->addworksheet(0);

    my $min_dest_file = Spreadsheet::WriteExcel->new($filename2)
	or die "Could not create a new Excel file: $!";
    my $min_dest_sheet = $min_dest_file->addworksheet(0);

    $dest_sheet->write($dest_row, $dest_col++, "SEQ_ID");
    $dest_sheet->write($dest_row, $dest_col++, "GENE_INFO");
    $dest_sheet->write($dest_row, $dest_col++, "SEQ_ID");
    $dest_sheet->write($dest_row, $dest_col++, "GENE_INFO");
    $dest_sheet->write($dest_row, $dest_col++, "Corr. Coeff.");   
    $dest_sheet->write($dest_row, $dest_col++, "M7Avr");
    $dest_sheet->write($dest_row, $dest_col++, "W7Avr");
    $dest_sheet->write($dest_row, $dest_col++, "M10Avr");
    $dest_sheet->write($dest_row, $dest_col++, "W10Avr");
    $dest_sheet->write($dest_row, $dest_col++, "M7Avr");
    $dest_sheet->write($dest_row, $dest_col++, "W7Avr");
    $dest_sheet->write($dest_row, $dest_col++, "M10Avr");
    $dest_sheet->write($dest_row, $dest_col++, "W10Avr");
    $dest_row++;

    $min_dest_sheet->write($min_dest_row, $min_dest_col++, "SEQ_ID");
    $min_dest_sheet->write($min_dest_row, $min_dest_col++, "GENE_INFO");
    $min_dest_sheet->write($min_dest_row, $min_dest_col++, "SEQ_ID");
    $min_dest_sheet->write($min_dest_row, $min_dest_col++, "GENE_INFO");
    $min_dest_sheet->write($min_dest_row, $min_dest_col++, "Corr. Coeff.");   
    $min_dest_row++;


    for (my $row = 2; $row < $sheet->{MaxRow} + 1; $row++) {

	print "Current Row: " . $row . "\n";

	@cur_ref_row = get_row($sheet, $row);

	@cur_row = @cur_ref_row;
	pop(@cur_row);
	shift(@cur_row);
	
	for (my $subsequent_row = $row + 1; $subsequent_row < $sheet->{MaxRow} + 1; $subsequent_row++) {

	    @ref_row = get_row($sheet, $subsequent_row);

	    if (@ref_row[$Info_element_position]) {

		@sub_row = @ref_row;
		pop(@sub_row);
		shift(@sub_row);

		$cur_row_stat_obj = Statistics::Descriptive::Full->new();
		$sub_row_stat_obj = Statistics::Descriptive::Full->new();
		
		$cur_row_stat_obj->add_data(@cur_row);
		$sub_row_stat_obj->add_data(@sub_row);
		
		$cur_row_mean = $cur_row_stat_obj->mean();
		$sub_row_mean = $sub_row_stat_obj->mean();
		
		$cur_row_std = $cur_row_stat_obj->standard_deviation();
		$sub_row_std = $sub_row_stat_obj->standard_deviation();
		
		$pcc = pearson_correlation_coefficient(\@cur_row, \@sub_row, $cur_row_mean, $sub_row_mean, $cur_row_std, $sub_row_std);
		
		if ($pcc > $threshold) 
		{

		    $dest_col = 0;

		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$At_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$Info_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$At_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$Info_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $pcc);
		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$M7Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$W7Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$M10Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $cur_ref_row[$W10Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$M7Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$W7Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$M10Avr_element_position]);
		    $dest_sheet->write($dest_row, $dest_col++, $ref_row[$W10Avr_element_position]);
		    
		    $dest_row++;
		    
		}

		elsif ($pcc <= $threshold2) 
		{

		    $min_dest_col = 0;
	    
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$At_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$Info_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$At_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$Info_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $pcc);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$M7Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$W7Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$M10Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $cur_ref_row[$W10Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$M7Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$W7Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$M10Avr_element_position]);
		    $min_dest_sheet->write($min_dest_row, $min_dest_col++, $ref_row[$W10Avr_element_position]);
		    
		    $min_dest_row++;
		    
		}
	    }
	}
    }
    
    $dest_file->close();
    $min_dest_file->close();
}

##############################################################
# Returns the values for each row
#############################################################
sub get_row
{
    my ($sheet, $row) = @_;

    my $min_col = 0;

    my @row_array = {};

    for (my $col = $min_col; $col < $sheet->{MaxCol} + 1; $col++) {

	my $cell = $sheet->{Cells}[$row][$col];
	
	$row_array[$col] = $cell->{Val};
	
    }

    return @row_array;
}

##############################################################
# Does a Pearson Correlation Coeffiecient against 2 rows
#############################################################
sub pearson_correlation_coefficient
{

    my ($cur_row, $sub_row, $cur_row_mean, $sub_row_mean, $cur_row_std, $sub_row_std) = @_;

    my @current_row = @{$cur_row};
    my @sub_row     = @{$sub_row};

    my $sum;

    $sum = 0;

    for (my $row = 0; $row < @current_row; $row++) 
    {

	$sum += ($current_row[$row] - $cur_row_mean) * ($sub_row[$row] - $sub_row_mean);
    }


    return $sum / ( (@current_row - 1) * $cur_row_std * $sub_row_std); 
    
}

