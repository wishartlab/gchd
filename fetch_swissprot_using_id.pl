#!/usr/bin/perl
#
#fetch_swissprot_using_id.pl
#version 1.0
#
#This script accepts a list of Swiss-Prot IDs. The sequence and title corresponding to each ID are 
#retrieved from ExPASy and written to a file in FASTA format. 
#
#There are two command line options:
#-i input file - a list of Swiss-Prot IDs separated by newlines.
#-o output file
#
#Sample input:
#
#P21926
#P30153
#
#Sample output:
#
#>P21926 CD9 antigen (P24) (Leukocyte antigen MIC3) (Motility-related protein)(MRP-1).
#PVKGGTKCIKYLLFGFNFIFWLAGIAVLAIGLWLRFDSQTKSIFEQETNNNNSSFYTGVYILIGAGALMMLVGFLGCCGAVQESQCMLGLFFGFLLVIFAIEIAAAIWGYSHKDEVIKEVQEFYKDTYNKLKTKDEPQRETLKAIHYALN#CCGLAGGVEQFISDICPKKDVLETFTVKSCPDAIKEVFDNKFHIIGAVGIGIAVVMIFGMIFSMILCCAIRRNREMV
#
#>P30153 Serine/threonine protein phosphatase 2A, 65 KDA regulatory subunit A,alpha isoform (PP2A, subunit A, PR65-alpha isoform) (PP2A, subunit A,R1-alpha isoform) (Medium tumor antigen-associated 61 KDA protein).
#AAADGDDSLYPIAVLIDELRNEDVQLRLNSIKKLSTIALALGVERTRSELLPFLTDTIYDEDEVLLALAEQLGTFTTLVGGPEYVHCLLPPLESLATVEETVVRDKAVESLRAISHEHSPSDLEAHFVPLVKRLAGGDWFTSRTSACGLF#SVCYPRVSSAVKAELRQYFRNLCSDDTPMVRRAAASKLGEFAKVLELDNVKSEIIPMFSNLASDEQDSVRLLAVEACVNIAQLLPQEDLEALVMPTLRQAAEDKSWAVRYMVADKFTELQKAVGPEITKTDLVPAFQNLMKDCEAEVRAA#ASHKVKEFCENLSADCRENVIMSQILPCIKELVSDANQHVKSALASVIMGLSPILGKDNTIEHLLPLFLAQLKDECPEVRLNIISNLDCVNEVIGIRQLSQSLLPAIVELAEDAKWRVRLAIIEYMPLLAGQLGVEFFDEKLNSLCMAWL#VDHVYAIREAATSNLKKLVEKFGKEWAHATIIPKVLAMSGDPNYLHRMTTLFCINVLSEVCGQDITTKHMLPTVLRMAGDPVANVRFNVAKSLQKIGPILDNSTLQSEVKPILEKLTQDQDVDVKYFAQEALTVLSLA
#
#
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk

use warnings;
use strict;

use LWP::UserAgent;
use HTTP::Request::Common;

#Create UserAgent object.
my $browser = LWP::UserAgent->new();
$browser->timeout(30);

#Command line processing.
use Getopt::Long;

my $inputFile;
my $outputFile;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'o|output_file=s' => \$outputFile);

if(!defined($inputFile)) {
    die ("Usage: fetch_swissprot_using_id.pl -i <input file> -o <output file>\n");
}

if(!defined($outputFile)) {
    die ("Usage: fetch_swissprot_using_id.pl -i <input file> -o <output file>\n");
}

#Now open the file specified by $inputFile
open(INFILE, $inputFile) or die( "Cannot open file for input: $!" );
open(OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");
close(OUTFILE) or die ("Cannot close file for output: $!");

#An entry index.
my $entryCount = 1;

while (my $line = <INFILE>) {
    if ($line =~ m/(\w+)\n/) {

	my $id = $1;
	my $title = "";
	my $sequence = "";

	print "Requesting sequence for entry $entryCount ($id).\n";
	my $response = $browser->request(POST ("http://us.expasy.org/cgi-bin/get-sprot-raw.pl?$id"));
	if ($response->is_success()) {

	    my $result = $response->as_string();

	    if ($result =~ m/^ID\s\s\s/m) {

		while ($result =~ m/^DE\s\s\s([^\n]+)/mg) {
		    my $titleSegment = $1;
		    $title = $title . $titleSegment;
		}

		if ($result =~ m/^SQ\s\s\s[^\n]+\n([^\/]+)\/\//m) {
		    $sequence = $1;
		    $sequence =~ s/[^A-Za-z]//g;
		}
		open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");
		print (OUTFILE ">" . $id . " " . $title . "\n" . $sequence . "\n\n");
		close(OUTFILE) or die ("Cannot close file for output: $!");

		print ("Success! reponse added to $outputFile\n");
	    }
	    else {
		open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");		
		print (OUTFILE ">" . $id . " " . $title . "\n" . "No record was received." . "\n\n");	
		close(OUTFILE) or die ("Cannot close file for output: $!");
		
		print ("No record was received for entry $entryCount ($id).\n");
	    }
	}
	else {
	    open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");	
	    print (OUTFILE ">" . $id . " " . $title . "\n" . "A response was not received." . "\n\n");
	    close(OUTFILE) or die ("Cannot close file for output: $!");	    

		
	    print ("A response was not received for entry $entryCount ($id).\n");
	}
	$entryCount = $entryCount + 1;
	sleep(1);
    }
}

close(INFILE) or die ("Cannot close file for output: $!");
