#!/usr/bin/perl
#retrieve_seq_v2.pl
#version 2
#
#Written by Paul Stothard, 
#Edited by Savita Shrivastava Canadian Bioinformatics Help Desk
#based on a script by Oleg Khovayko http://olegh.spedia.net
#
#This script uses NCBI's Entrez Programming Utilities to perform
#batch requests to NCBI Entrez.
#
#Values in the 'edit these' section should be changed to meet your requirements
#
#See 'Entrez Programming Utilities' for more info at
#http://eutils.ncbi.nlm.nih.gov/entrez/query/static/eutils_help.html


use warnings;
use strict;
use LWP::Simple;
use Bio::SeqIO;
#--------------edit these-----------

# Here provide the name of the organism for which you want to
# download the sequences. Do not remove "[Organism]". Just change the name
# of the organism where "Cytophaga" is written. Save the file and run 
# as "perl retrieve_seq_v2.pl"

my $query = "Cytophaga[Organism]";

# output file where you want to store the sequences
my $output_file = "cytophaga";
my $db = "nucleotide";
my $report = "gb";
#-----------------------------------

my $url = "http://www.ncbi.nlm.nih.gov/entrez/eutils";
my $esearch = "$url/esearch.fcgi?" . "db=$db&retmax=1&usehistory=y&term=";

my $esearch_result = get($esearch . $query);

$esearch_result =~ m/<Count>(\d+)<\/Count>.*<QueryKey>(\d+)<\/QueryKey>.*<WebEnv>(\S+)<\/WebEnv>/s;

my $count = $1;
my $query_key = $2;
my $web_env = $3;

my $retstart;
my $retmax = 500;

open (OUTFILE, ">" . $output_file) or die ("Error: Cannot open $output_file : $!");

print "$count entries to retrieve\n";

for ($retstart = 0; $retstart < $count; $retstart = $retstart + $retmax) {
    print "Requesting entries $retstart to " . ($retstart + $retmax) . "\n";
    my $efetch = "$url/efetch.fcgi?" . "rettype=$report&retmode=text&retstart=$retstart&retmax=$retmax&" .
	"db=$db&query_key=$query_key&WebEnv=$web_env";
    
    my $efetch_result = get($efetch);
    
    print (OUTFILE $efetch_result);
    sleep(3);
}

close (OUTFILE) or die( "Error: Cannot close $output_file file: $!");

#----------------------------------------------------------------------

# Separating each 16S rRNA sequence into an individual file
# sorting sequence based on there length
# if the sequence is less than 1500 then it will go to directory
# named "<outputfilename>_less_than_1500" and if length is greater than
# 1500 then the file will go in the directory named
# "<outputfilename>_greater_than_1500"
#-----------------------------------------------------------------------

my $lengthDir1 = "$output_file"."_less_than_1500";
my $lengthDir2 = "$output_file"."_greater_than_1500";
unless (-e $lengthDir1){ mkdir $lengthDir1};
unless (-e $lengthDir2){ mkdir $lengthDir2};

my $inC = Bio::SeqIO->new(-file => $output_file, -format => 'genbank');

my @featureCopy = ();
my $lengthB;
print "\nSorting 16S rRNA sequences\n";
while(my $seqB = $inC->next_seq()){
    my $rnatype;
    $lengthB = $seqB->length();
    
    my $name = $seqB->display_id;
    print "$name\n";
    
    #need to get the features from the GenBank record.
    my @features = $seqB->get_SeqFeatures();
    
    
    foreach (@features) {
	my $type = lc($_->primary_tag);
	#shouldn't be any scRNA or snRNA in bacteria
	if ($type eq "rrna") {
	    $rnatype = $type;
	    push @featureCopy, $_;
	    
	}
    }
    if($rnatype){
	my $gene_flag;
	my $protein_flag;
	
	foreach (@featureCopy) { 
	    my $feat = $_;
	    my $gene_name;
	    my $protein_name;
	    if($feat->has_tag('gene')) {
		$gene_name = join("",$feat->get_tag_values('gene'));
	    }
	    if($feat->has_tag('product')){
		$protein_name = join("", $feat->get_tag_values('product'));
	    }
	    if ($gene_name){
		if($gene_name =~ /16S\s+rRNA/){
		    $gene_flag =1;
		}
	    }
	    
	    if($protein_name){
		if($protein_name =~/16S\s+ribosomal\s+RNA/){
		    $protein_flag =1;
		}
	    }
	    if($gene_flag || $protein_flag){
		
		if($lengthB <=1500){
		    # writing Single entry into a single file named on sequence id
		    my $out = Bio::SeqIO->new(-file => ">cytophaga_less_than_1500/$name", -format => 'genbank');
		    $out->write_seq($seqB);
		}
		elsif($lengthB>=1500){
		    # writing Single entry into a single file named on sequence id
		    my $out = Bio::SeqIO->new(-file => ">cytophaga_greater_than_1500/$name", -format => 'genbank');
		    $out->write_seq($seqB);
		    
		}
	    }
	}
    }
}
print "\nSorting 16S rRNA sequences is finished\n";


#=== end of script
    






