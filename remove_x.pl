#!/usr/bin/perl
#remove_x.pl
#Version 1.0
#Reads multiple sequence files in FASTA format from a file and removes X's and x's from the sequences.
#Any sequences less than $MIN_SIZE bases are not written out.
#The resulting files are written to an output file.

#There are two command line options:
#-i input file
#-o output file
#
#example usage:
#perl remove_x.pl -i my_sequences.txt -o my_output.txt
#
#Written by Paul Stothard, Canadian Bioinformatics Help Desk.
#
#stothard@ualberta.ca

use warnings;
use strict;

#Command line processing.
use Getopt::Long;

my $MIN_SIZE = 200;

my $inputFile;
my $outputFile;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'o|output_file=s' => \$outputFile);

if(!defined($inputFile)) {
    die ("Usage: remove_x.pl -i <input file> -o <output file>\n");
}

if(!defined($outputFile)) {
    die ("Usage: remove_x.pl -i <input file> -o <output file>\n");
}

#Make counter for sequences.
my $seqCount = 0;
my $skippedSeq = 0;
my $removedX = 0;

open (OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");

open (SEQFILE, $inputFile) or die( "Cannot open file : $!" );
$/ = ">";

#read each sequence.
while (my $sequenceEntry = <SEQFILE>) {

    if ($sequenceEntry eq ">"){
	next;
    }

    my $sequenceTitle = "";
    if ($sequenceEntry =~ m/^([^\n]+)/){
	$sequenceTitle = $1;
    }
    else {
	$sequenceTitle = "No title was found!";
    }

    $sequenceEntry =~ s/^[^\n]+//;
    $sequenceEntry =~ s/[^A-Za-z]//g;

    my $oldLength = length($sequenceEntry);

    #now replace X characters with nothing
    $sequenceEntry =~ s/[Xx]//g;  

    $removedX = $removedX + ($oldLength - length($sequenceEntry));

    if (length($sequenceEntry) < $MIN_SIZE) {
	$skippedSeq++;
    }
    else {
	print (OUTFILE ">$sequenceTitle\n");
	print (OUTFILE "$sequenceEntry\n\n");
    }

    $seqCount = $seqCount + 1;

}#end of while loop

close (SEQFILE) or die( "Cannot close file : $!");
close (OUTFILE) or die( "Cannot close file : $!");
print "A total of $seqCount records were read.\n";
print "A total of $removedX X's were removed.\n";
print "$skippedSeq of the records were skipped because they are less than $MIN_SIZE bases.\n";
