#!/usr/bin/perl
#remove_duplicates.pl
#Version 1.0
#Reads multiple sequence files in FASTA format from a file and keeps track of the titles.
#If there are two or more titles that match (either completely or just gi number) only
#the first record in the matching group is written to the output file. All records with
#unique titles are written to the output file.

#There are two command line options:
#-i input file
#-o output file
#
#example usage:
#perl remove_duplicates.pl -i my_sequences.txt -o my_output.txt
#
#Written by Paul Stothard, Canadian Bioinformatics Help Desk.
#
#stothard@ualberta.ca

use warnings;
use strict;

#Command line processing.
use Getopt::Long;

my $inputFile;
my $outputFile;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'o|output_file=s' => \$outputFile);

if(!defined($inputFile)) {
    die ("Usage: remove_duplicates.pl -i <input file> -o <output file>\n");
}

if(!defined($outputFile)) {
    die ("Usage: remove_duplicates.pl -i <input file> -o <output file>\n");
}

#Make counter for sequences.
my $seqCount = 0;
my $skippedSeq = 0;
my @alreadyWritten = ();

open (OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");

open (SEQFILE, $inputFile) or die( "Cannot open file : $!" );
$/ = ">";

#read each sequence.
while (my $sequenceEntry = <SEQFILE>) {

    my $duplicate = 0;

    if ($sequenceEntry eq ">"){
	next;
    }

    my $sequenceTitle = "";
    if ($sequenceEntry =~ m/^([^\n]+)/){
	$sequenceTitle = $1;
    }
    else {
	$sequenceTitle = "No title was found!";
    }

    $sequenceEntry =~ s/^[^\n]+//;
    $sequenceEntry =~ s/[^A-Za-z]//g;

    foreach(@alreadyWritten) {
	my $previousTitle = $_;

	if ($previousTitle eq $sequenceTitle) {
	    $duplicate = 1;
	    last;
	}

	if ($previousTitle =~ m/gi\|(\d+)/) {
	    my $previousGi = $1;
	    if ($sequenceTitle =~ m/gi\|(\d+)/) {
		my $currentGi = $1;
		if ($previousGi eq $currentGi) {
		    $duplicate = 1;
		    last;
		}
	    }
	}
    }

    if (!($duplicate)) {
	print (OUTFILE ">$sequenceTitle\n");
	print (OUTFILE "$sequenceEntry\n\n");
	push (@alreadyWritten, $sequenceTitle);
    }
    else {
	$skippedSeq++;
    }

    $seqCount = $seqCount + 1;

}#end of while loop

close (SEQFILE) or die( "Cannot close file : $!");
close (OUTFILE) or die( "Cannot close file : $!");
print "A total of $seqCount records were read.\n";
print "$skippedSeq of the records were skipped because they were present more than once based on title.\n";
