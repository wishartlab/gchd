#!/usr/bin/perl
#
#fetch_protein.pl
#version 2.0
#
#This script accepts a list of Swiss-Prot IDs or Swiss-Prot names. The sequence record 
#corresponding to each ID is retrieved from ExPASy and written to a separate 
#file in the output directory you specify. Records can be written in FASTA
#format or in swissprot format. 
#
#There are three command line options:
#
#-i input file - a list of Swiss-Prot IDs or names separated by newlines.
#-o output directory - directory to contain the sequence records
#-f format - fasta, or swissprot. Default is fasta
#
#example usage:
#
#perl fetch_protein.pl -i id_list.txt -o sequences -f swissprot
#
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk

use warnings;
use strict;

use LWP::UserAgent;
use HTTP::Request::Common;

#Create UserAgent object.
my $browser = LWP::UserAgent->new();
$browser->timeout(30);

#Command line processing.
use Getopt::Long;

my $inputFile;
my $outputDirectory;
my $format;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'o|output_directory=s' => \$outputDirectory,
	    'f|format=s' => \$format);

if(!defined($inputFile)) {
    die ("Usage: fetch_protein.pl -i <input file> -o <output directory> -f <format>\n");
}

if(!defined($outputDirectory)) {
    die ("Usage: fetch_protein.pl -i <input file> -o <output directory> -f <format>\n");
}

if ($format =~ m/swiss/i) {
    $format = "swissprot";
}
else {
    $format = "fasta";
}

#Now open the file specified by $inputFile
open(INFILE, $inputFile) or die( "Cannot open file for input: $!" );

#make output directory if it doesn't exist
if (!(-d $outputDirectory)) {
    mkdir ($outputDirectory, 0777);
}

#An entry index.
my $entryCount = 1;

while (my $line = <INFILE>) {
    if ($line =~ m/(\w+)\n/) {

	my $id = $1;
	my $title = "";
	my $sequence = "";

	print "Requesting sequence for entry $entryCount ($id).\n";
	my $response = $browser->request(POST ("http://us.expasy.org/cgi-bin/get-sprot-raw.pl?$id"));
	if ($response->is_success()) {

	    my $result = $response->as_string();

	    if ($result =~ m/^ID\s\s\s/m) {
		my $recordToWrite;
		
		if ($format eq "fasta") {
		    while ($result =~ m/^DE\s\s\s([^\n]+)/mg) {
			my $titleSegment = $1;
			$title = $title . $titleSegment;
		    }

		    if ($result =~ m/^SQ\s\s\s[^\n]+\n([^\/]+)\/\//m) {
			$sequence = $1;
			$sequence =~ s/[^A-Za-z]//g;
		    }

		    $recordToWrite = ">" . $id . " " . $title . "\n" . $sequence . "\n";
		}
		else {
		    if ($result =~ m/(^ID\s\s\s[\s\S]+^\/\/)/m) {
			$recordToWrite = $1;	
		    }
		    else {
			print ("Warning: Could not remove header info from entry $entryCount ($id).\n");
			$recordToWrite = $result;	
		    }
		}

		open(OUTFILE, ">" . $outputDirectory . "/" . $id . "." . $format) or die ("Cannot open file for output: $!");
		print (OUTFILE $recordToWrite);
		close(OUTFILE) or die ("Cannot close file for output: $!");

		print ("Success! sequence added to " . $outputDirectory . "\n");
	    }
	    else {
		open(OUTFILE, ">" . $outputDirectory . "/" . $id . "." . $format) or die ("Cannot open file for output: $!");	
		print (OUTFILE $id . " " . $title . "\n" . "No record was received." . "\n");	
		close(OUTFILE) or die ("Cannot close file for output: $!");
		
		print ("No record was received for entry $entryCount ($id).\n");
	    }
	}
	else {
	    open(OUTFILE, ">" . $outputDirectory . "/" . $id . "." . "format") or die ("Cannot open file for output: $!");	
	    print (OUTFILE $id . " " . $title . "\n" . "A response was not received." . "\n");
	    close(OUTFILE) or die ("Cannot close file for output: $!");	    

		
	    print ("A response was not received for entry $entryCount ($id).\n");
	}
	$entryCount = $entryCount + 1;
	sleep(1);
    }
}

close(INFILE) or die ("Cannot close file for output: $!");
