#!/usr/bin/perl -w

# Given a file containing multiple FASTA-formatted entries, this script outputs
# a file containing only the FASTA headers.
#
# David Arndt, July 2007

use strict;

sub trim($);

################################################################
##########                 Main code                 ###########
################################################################

if(@ARGV<1)
{
  print "Usage: ./extract_FASTA_headers.pl <database_file>\n";
  print "   Note: <database_file> is a file with multiple FASTA-formatted\n";
  print "   entries.\n";
  exit(0);
}

my $db_filename = $ARGV[0];

my $db_data = slurp($db_filename);

open(OUT, ">$db_filename\.headers");

while ( $db_data =~ /^[>#](.*)/mg ) {
	print OUT $1;
	print OUT "\n";
}

close(OUT);

print "Headers file $db_filename\.headers created.\n";


################################################################
##########                 Functions                 ###########
################################################################

# http://b3thm00n.com/scripting/perl_slurp/
# Read a file into a string.
sub slurp{
        my $file=$_[0];
	
	#passed the absolute path on machine to the file you want to read

	unless (-f $file){ return 0; } #if it does not exist 
		#return false
		
	my $src;

        local *INPUT;
        open (INPUT, $file);
        my $chunk;
        for (;;) {
                read(INPUT, $chunk, 1024); #how much to read per cycle (1k)
                last if (length($chunk) == 0); #if nothing was read then  stop
                $src.=$chunk;
        }
        close INPUT;
	
        return $src;
}
