#!/usr/bin/perl -w

# A script to reformat unusual PDB files into a more standard PDB format. This
# script (1) re-orders the atoms within each residue into a 'standard' order, (2)
# renames atoms to a 'standard' format, e.g. HD23 becomes 3HD2, (3) renames
# certain residues, e.g. 'HSD' or 'HID' become 'HIS', (4) preserves only one
# location for each atom, for atoms that have alternate location codes.
#
# This script was originally written for the unusual PDB format used for RECOORD
# structures (http://www.ebi.ac.uk/msd-srv/docs/NMR/recoord/main.html).
#
# Note: This script assumes that lines beginning with 'ATOM' are not interspersed
# with lines that do not begin with 'ATOM'.
#
# See http://www.biochem.ucl.ac.uk/~roman/procheck/manual/manappb.html
# for PDB file format info.
#
# Version 1.0
# Author: David Arndt, Jan 2, 2007

use strict;

use constant TYPE_NON_HYDROGEN => 1;
use constant TYPE_HYDROGEN => 2;

if(@ARGV<2)
{
  print "Usage: ./reformat_PDB_file.pl <input PDB file> <output PDB file>\n";
  exit(0);
}

open(IN,"$ARGV[0]") || die "** reformat_PDB_file.pl cannot open file $ARGV[0]\n";
my @data=<IN>;
close(IN);

open(OUT, ">$ARGV[1]");

my @non_hydrogen_lines = ();
my @hydrogen_lines = ();
my $non_hydrogen_index = 0;
my $hydrogen_index = 0;

my $started_first_residue = 0;
my $non_atom_line = 0;
my $end_of_file = 0;
my $output_prev_res = 0;
my $res_num;
my $prev_res_num;
my $current_chain_code;
my $prev_chain_code;
my $first_atom_num;
my $atom_counter = 0;

for (my $line_num = 0; $line_num < scalar(@data) + 1; $line_num++) {
	
	my $line = "";
	if ($line_num < scalar(@data)) {
		$line = $data[$line_num];
		
		if (substr($line, 0, 4) =~ 'ATOM') {
			$non_atom_line = 0;
		}
		else {
			$non_atom_line = 1;
		}
		
		if (!$started_first_residue) {
			if ($non_atom_line == 0) {
				# The current line is for the first atom in the PDB file.
				
				$started_first_residue = 1;
				
				if (substr($line, 22, 4) =~ /(\d+)/) {
					$res_num = $1;
				}
				
				if (substr($line, 6, 5) =~ /(\d+)/) {
					$first_atom_num = $1;
				}
				
				$current_chain_code = substr($line, 21, 1);
			}
			else {
				# Header line; it will be printed below.
			}
		}
		else {
			if (!$non_atom_line) {
				$prev_res_num = $res_num;
				
				if (substr($line, 22, 4) =~ /(\d+)/) {
					$res_num = $1;
				}
				
				if ($prev_res_num != $res_num) {
					$output_prev_res = 1;
				}
				
				# Check for the beginning of a new model. If we're starting a new model,
				# we want to make sure that atom numbers will restart with the first atom
				# number of the residue. However, if we're starting a new chain instead,
				# we do not want to reset the atom numbers.
				
				$prev_chain_code = $current_chain_code;
				$current_chain_code = substr($line, 21, 1);
				if ($started_first_residue) {
					if ( ($prev_res_num > $res_num) && ($current_chain_code eq $prev_chain_code) ) {
						if (substr($line, 6, 5) =~ /(\d+)/) {
							$first_atom_num = $1;
						}
						$atom_counter = 0;
					}
				}
			}
			else {
				$output_prev_res = 1;
			}
		}
	}
	else {
		# End of file reached, but we have not output the last residue yet,
		# so we will do that.
		
		$end_of_file = 1;
		$output_prev_res = 1;
	}
	
	
	if ($output_prev_res) {
		# We are done parsing the previous residue, so output the
		# data for the previous residue -- the non-hydrogen atoms
		# first followed by the hydrogen atoms.
		
		my $the_line;
		my $found_first_backbone_O = 0;
		
		# Re-order non-hydrogen atoms. Put N, CA, C, O first (in that order),
		# then the rest, except for OXT which is last.
		
		my @non_hydrogen_lines_reordered = ();
		my $non_hydrogen_reordered_index = 0;
		
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ($atom_name =~ / N  /) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
				last;
			}
		}
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ($atom_name =~ / CA /) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
				last;
			}
		}
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ($atom_name =~ / C  /) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
				last;
			}
		}
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ($atom_name =~ / O  /) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
				last;
			}
		}
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ( !(($atom_name =~ / N  /) || ($atom_name =~ / CA /) || ($atom_name =~ / C  /)
					|| ($atom_name =~ / O  /) || ($atom_name =~ / OXT/)) ) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
			}
		}
		foreach $the_line(@non_hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			
			if ($atom_name =~ / O  /) {
				if (!$found_first_backbone_O) {
					$found_first_backbone_O = 1;
				}
				else {
					# We found a second backbone O atom. We infer that it must be a terminal OXT atom.
					# Some PDB files call this atom 'O' instead of 'OXT'. We want it to be called 'OXT'.
					$atom_name = ' OXT';
					substr($the_line, 12, 4) = $atom_name; # Fix atom name in $the_line.
				}
			}
			
			if ($atom_name =~ / OXT/) {
				$non_hydrogen_lines_reordered[$non_hydrogen_reordered_index] = $the_line;
				$non_hydrogen_reordered_index++;
				last;
			}
		}
		
		# Re-order the hydrogen atoms. Put 1H, 2H, 3H first, then the others.
		
		my @hydrogen_lines_reordered = ();
		my $hydrogen_reordered_index = 0;
		
		foreach $the_line(@hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if ($atom_name =~ /\dH  /) {
				$hydrogen_lines_reordered[$hydrogen_reordered_index] = $the_line;
				$hydrogen_reordered_index++;
			}
		}
		foreach $the_line(@hydrogen_lines) {
			my $atom_name = substr($the_line, 12, 4);
			if (!($atom_name =~ /\dH  /)) {
				$hydrogen_lines_reordered[$hydrogen_reordered_index] = $the_line;
				$hydrogen_reordered_index++;
			}
		}
		
		# Output the data for the previous residue. Assign new atom numbers so that
		# the output file has atoms numbered in order.
		
		my $output_line;
		foreach $output_line(@non_hydrogen_lines_reordered) {
			substr($output_line, 6, 5) = sprintf("%5s", ($first_atom_num + $atom_counter));
			print(OUT $output_line);
			$atom_counter++;
		}
		foreach $output_line(@hydrogen_lines_reordered) {
			substr($output_line, 6, 5) = sprintf("%5s", ($first_atom_num + $atom_counter));
			print(OUT $output_line);
			$atom_counter++;
		}
		
		# Reset variables.
		
		@non_hydrogen_lines = ();
		@hydrogen_lines = ();
		$non_hydrogen_index = 0;
		$hydrogen_index = 0;
		$output_prev_res = 0;
	}
	
	if (!$end_of_file) {
		
		if ($non_atom_line) {
			
			# Adjust the atom number in TER lines.
			if (substr($line, 0, 4) =~ 'TER ') {
				substr($line, 6, 5) = sprintf("%5s", ($first_atom_num + $atom_counter));
				$atom_counter++;
			}
			
			# Output the non-atom line.
			
			print(OUT $line);
		}
		
		
		if (!$non_atom_line) {
			
			# Check the alternate location code. If there is an alternate location code,
			# only include atoms with code 'A', and remove the alternate location code.
			
			my $alt_loc_code = substr($line, 16, 1);
			
			if ($alt_loc_code ne ' ') {
				if ($alt_loc_code eq 'A') {
					substr($line, 16, 1) = ' ';
				}
				else {
					next; # Skip this line.
				}
			}
			
			# Replace residues named 'HSD' or 'HID' with 'HIS', and 'CYX' with 'CYS'.
			# 'HSD' is used in NAMD files.
			
			my $res_name = substr($line, 17, 3);
			
			if ($res_name =~ /HSD/) {
				substr($line, 17, 3) = 'HIS'; # Fix residue name in original input line.
			}
			elsif ($res_name =~ /HID/) {
				substr($line, 17, 3) = 'HIS'; # Fix residue name in original input line.
			}
			elsif ($res_name =~ /CYX/) {
				substr($line, 17, 3) = 'CYS'; # Fix residue name in original input line.
			}
			
			# Examine the atom name to indentify whether it is a hydrogen
			# or not, and fix any unusual data formatting.
			
			my $atom_name = substr($line, 12, 4);
			my $atom_type;
			
			if ($atom_name =~ /(H[A-Z]\d)(\d)/) {
				$atom_name = $2 . $1;
				$atom_type = TYPE_HYDROGEN;
			}
			elsif ($atom_name =~ / HT(\d)/) { # N-terminal hydrogens.
				$atom_name = $1 . 'H  ';
				$atom_type = TYPE_HYDROGEN;
			}
			#elsif ($atom_name =~ / (H[A-Z])(\d)/) {
			#	$atom_name = $2 . $1 . ' ';
			#	$atom_type = TYPE_HYDROGEN;
			#}
			elsif ($atom_name =~ / HN /) {
				$atom_name = ' H  ';
				$atom_type = TYPE_HYDROGEN;
			}
			elsif ($atom_name =~ /.H../) {
				$atom_type = TYPE_HYDROGEN;
			}
			elsif ($atom_name =~ / OT1/) { # First C-terminal O.
				$atom_name = ' O  ';
				$atom_type = TYPE_NON_HYDROGEN;
			}
			elsif ($atom_name =~ / OT2/) { # Second C-terminal O.
				$atom_name = ' OXT';
				$atom_type = TYPE_NON_HYDROGEN;
			}
			elsif ($atom_name =~ / O1 /) { # First C-terminal O.
				$atom_name = ' O  ';
				$atom_type = TYPE_NON_HYDROGEN;
			}
			elsif ($atom_name =~ / O2 /) { # Second C-terminal O.
				$atom_name = ' OXT';
				$atom_type = TYPE_NON_HYDROGEN;
			}
			else {
				$atom_type = TYPE_NON_HYDROGEN;
			}
			
			
			# Store the current input line in one of two arrays. We keep track of
			# non-hydrogen atoms and hydrogen atoms separately so that we can output
			# them in standard order (non-hydrogens followed by hydrogens).
			substr($line, 12, 4) = $atom_name; # Fix atom name in original input line.
			if ($atom_type == TYPE_NON_HYDROGEN) {
				$non_hydrogen_lines[$non_hydrogen_index] = $line;
				$non_hydrogen_index++;
			}
			elsif ($atom_type == TYPE_HYDROGEN) {
				$hydrogen_lines[$hydrogen_index] = $line;
				$hydrogen_index++;
			}
		}
	}
	
}

close(OUT);
