#!/usr/bin/perl
# go_fish_source.pl
# Author: Gary Van Domselaar, Canadian Bioinformatics Help Desk
# This script reads multiple blast results and tries to assign existing
# GO annotations.
#
# This program will only give results for blast results below a maxiumum
# e-value.

# use warnings;
use strict;

# the following paths must be set correctly to run the program
# edit the file names to provide the latest database names
my $PROCESS_ONTOLOGY = "process.ontology.2004-05-01";
my $FUNCTION_ONTOLOGY = "function.ontology.2004-05-01";
my $COMPONENT_ONTOLOGY = "component.ontology.2004-05-01";
my $GOA_SPTR = "gene_association.goa_uniprot";
my $GOA_PDB = "gene_association.goa_pdb";
my $GOA_GB = "gene_association.Compugen_GenBank";

# Command line processing
my $input_file;
my $output_file;
my $expectation_value ;

use Getopt::Long;

Getopt::Long::Configure ('bundling');
GetOptions (
            'i|input_file=s' => \$input_file,
            'o|output_file=s' => \$output_file,
            'e|expectation_value=i' => \$expectation_value);
if (!defined($input_file)  ) {
    die "Usage: go_fish_source.pl -i <input file> -o <output_file> -e <expectation_value> \n";
}

if (!defined($expectation_value)) {
    $expectation_value=10;
    print STDERR "Using max expectation value e-10.\n";
}
if (!defined($output_file)) {
    print STDERR "Using go_fish.out as output file\n";
    $output_file = "go_fish.out\n";
}

open (INPUT, $input_file) || die "Could not open the input file: $input_file.  $!\n";
open (OUTPUT, ">$output_file") || die "Could not open the output file: $output_file.  $!\n";

# Step one.  make a hash out of all blast entries from the annotator.pl file
# the index uses the contig name as a key and an array of unique gi numbers as 
# the value
# ie. SHContig => [gi|xxxx|associated acc num] [gi|yyy|associated accession num] etc...

# A blast entry looks like this:
# Fields: Query id, Subject id, % identity, alignment length, mismatches, 
# gap openings, q. start, q. end, s. start, s. end, e- value, bit score
# example:
# SHContig_1      gi|3121909|sp|Q35826|COX3_SPOFR 82.08   106     
# 19      0       372     689     13      118     4.9e-52 186.4 

# 0   Query id
# 1   Subject id
# 2   % identity
# 3   alignment length
# 4   mismatches
# 5   gap openings
# 6   q. start
# 7   q. end
# 8   s. start
# 9   s. end
# 10  e- value
# 11  bit score

my %h_contig;
my $last_key;
my @blast_val;
my %gi_numbers;
my @infile;
print STDERR "Building blast result index...\n";
while (<INPUT>) {
   
    chomp;
    push @infile, $_;
    if (/^\#/) {
	next;
    }
     @blast_val = split (/\t/,$_);
    
    # TEST: there should be exactly 12 values
    my $num_vals =  scalar(@blast_val);
   
    if(!$num_vals==12) { next;}
    
    #TEST: e value is less than minimum;
    if(!defined($blast_val[10])) {next;}
    if(!($blast_val[10] =~ /e-(\d+)/)) {next;}
   
    if ($1<= $expectation_value) {next;}
    
    #build an array of unique gi numbers for each contig
    if (!($blast_val[0] eq $last_key)) {

	if (defined($last_key)) {
	    my @gi_numbers =  keys(%gi_numbers);
	    $h_contig{$last_key} = \@gi_numbers;
	    %gi_numbers = ();
	    
	}
	$gi_numbers{$blast_val[1]} = 1;
	$last_key = $blast_val[0];
    }
    else {
	
	$gi_numbers{$blast_val[1]} = 1;
	
	
    }
    
}
my @gi_numbers = keys(%gi_numbers);
	    $h_contig{$last_key} = \@gi_numbers;



#
# This section builds the indexes for the accession numbers:GO numbers
# and the index for the GO numbers: Go descriptions
#

# Step 1. build a hash from the genbank gene association file.
# the file looks like:
# CGEN PrID69417 10 GO:0016538 CGEN:ProdVersion0.5.1 IEA F protein taxon:991320020703 
# index 2 contains the gi number
# index 3 contains the go number
my %h_gengo;
my @a_go;
my $last_id;
my @gengo_line;
print STDERR "Building genbank GO association index...\n";
open (GENGO, $GOA_GB)|| die "Cant open $GOA_GB: $!\n"; ;
while (<GENGO>) {
    @gengo_line = split (/\t+/, $_);
    my $current_id = $gengo_line[2];
    $h_gengo{$current_id} .= "\t$gengo_line[3]\n";
}
close GENGO;

# Step 2. build a hash for the swissprot/trembl gene association file.
#
# The file looks like:
# SPTR O00050 O00050 GO:0003676  GOA:interpro IEA  F  Transposase protein 
# taxon:105351 200308 12  SPTR
# index 1 contains the swissprot accession
# index 3 contains the go number
open (SPGO, $GOA_SPTR) || die "Cant open $GOA_SPTR: $!\n";
my @spgo_line;
my %spgo;
print "Building SP/TREMBL GO association index...\n";
while (<SPGO>) {
    my @spgo_line = split (/\t+/, $_);
    $spgo{$spgo_line[1]} .= "\t$spgo_line[3]\n";
}
close SPGO;


# Step 3. Build a hash for the pdb database
#
#
# The file looks like:
# PDB 1H3O C GO:0005669 GOA:manual IEA SPTR:O00268 C protein_structure taxon:9606 20010711
# index 1&2 contains the pdb code
# index 2 contains the go number   
my %h_pdbgo;
print STDERR "Building PDB GO association index...\n";
open PDBGO,   $GOA_PDB || die "Cant open $GOA_PDB: $!\n";
while (<PDBGO>) {
    my @pdbgo_line = split (/\t+/, $_);
    my $pdb_id = join("|", $pdbgo_line[1],$pdbgo_line[2]);
    $h_pdbgo{$pdb_id}  .= "\t$pdbgo_line[3]\n";
}
close PDBGO;

# Step 5: generate GO index from ontology files
# the file looks like
# <bud ; GO:0005933
#    <bud neck ; GO:0005935 % site of polarized growth (sensu Saccharomyces) ; GO:0000134
#     <contractile ring (sensu Saccharomyces) ; GO:0000142 ; synonym:cytokinetic ring (sensu Saccharomyces) ; synonym:neck ring % contractile ring (sensu Fungi) ; GO:0030480
# the < means 'part of'
# the % means IS A
# 
# I match anything between a <,%,or ; and ;GO<numbers>
# $1 will contain the description
# $2 will contain the go number
my @file;
print STDERR "Building GO description index...\n";
$file[0] = $COMPONENT_ONTOLOGY; # $i=1
$file[1] = $FUNCTION_ONTOLOGY; # $i=2
$file[2] = $PROCESS_ONTOLOGY; # $i=3

my %go;
my $filename;
my $source;

my $i = 1; # counter set at 1
foreach $filename (@file) {
    if($i==1) {
	$source = "CELLULAR COMPONENT";
    }
    if($i==2) {
	$source = "MOLECULAR FUNCTION";
    }
    if($i==3) {
	$source = "BIOLOGICAL PROCESS";
    }
    open (GO, $filename) ;
    while (<GO> ) {
	/[<|\%|\;]([^\;].*?)\;\s(GO:\d+)/g;
	if (defined($2)) {
	    $go{$2} = "$source: $1";
	}
    }
    close GO;
    $i++;
}

#	    
# Step 6.  Go through every entry and get the genbank record, then parse the genbank 
# record looking for GO annotations.
#

my $key;

my %h_result;  # this records the gi numbers that have a result
print STDERR "Building output file...\n";
foreach $key (%h_contig) {
    my  $r_list = $h_contig{$key};
    my $list_entry;
   
    GINUMS:foreach $list_entry (@{$r_list}) {
      
     
      # this will get the database of origin(gb|sp|emb|etc...)
      $list_entry =~ /gi\|(\d+)\|([^\|].*?)\|([^\|].*)$/;
      # get the gi number
      my $gi_number = $1;
      my $index_selector = $2; # the original sequence database
      if ($2 eq "gb" || $2 eq "dbj" || $2 eq "ref" || $2 eq "emb" ) {
	  if (defined($h_gengo{$gi_number}) ){
	      $h_result{$gi_number} .= " $h_gengo{$gi_number} ";
	  }
	  
      }
      # this will handle the swissprot/trembl entries
      if ($2 eq 'sp' || $2 eq "emb") {
	  # get all the sp accession numbers
	  my @sp_ids = split (/\|/, $3);
	  my $sp_id;
	  foreach $sp_id (@sp_ids) {
	      if(defined($spgo{$sp_id})) {
		  # diagnostic
		  # print "$key\t$gi_number\t$2 (id: $sp_id)\t$spgo{$sp_id}\n";
		   $h_result{$gi_number} .= " $spgo{$sp_id} ";
	      }
	      
	  }
	  
      }
      if ($2 eq  "pdb") {
	  my $pdb_id = $3;
	  chomp($pdb_id);
	  # get the pdb code and chain as code|chain
	  if ($pdb_id=~/(^[^\s].*)\s+$/) {
	      $pdb_id =~ tr/ /@/;
	  }
	  # diagnostic
	  # print "$key\t$gi_number\t$2 (id: $pdb_id)\t$h_pdbgo{$pdb_id}\n";
	  if (defined($h_pdbgo{$pdb_id})) {
	      $h_result{$gi_number} .= " $h_pdbgo{$pdb_id} ";
	  }
	  
      }
      
  }
    
}

# print out everything with a result
my $hit_counter;
my $miss_counter;
foreach $key (%h_contig) {
    my  $r_list = $h_contig{$key};
    my $list_entry;
    
    foreach $list_entry (@{$r_list}) {
	my @list_parts = split (/\|/, $list_entry);
	if (defined($h_result{$list_parts[1]})) {
	    print OUTPUT "\n$key\t$list_parts[1]";
	    my @GOs = split (/\s+/, $h_result{$list_parts[1]});
	    my $go_id;
	    # get the go description for each GO:number
	    foreach $go_id (@GOs) {
		my $go_desc = $go{$go_id};
	
		print  OUTPUT "\t$go_id\t$go_desc\n";
	    }
	    $hit_counter++;
	}
    }
}

# print out everything without a result (go number)
foreach $key (%h_contig) {
    my  $r_list = $h_contig{$key};
    my $list_entry;
    foreach $list_entry (@{$r_list}) {
	my @list_parts = split (/\|/, $list_entry);
	if (!defined($h_result{$list_parts[1]})) {
	   
	     print OUTPUT "$key\t$list_entry\t No GO\n";
	    $miss_counter++;
	}
	
    }
}

print STDERR "Found GO numbers for $hit_counter gi accession numbers\n";
print STDERR  "Missed $miss_counter gi_numbers\n";
