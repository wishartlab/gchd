#!/usr/bin/perl
#genome_search.pl
#version 1.0
#Reads a genomic sequence in FASTA format from a file and searches for patterns.
#The position of each pattern and the exact sequence that matched the pattern
#are written to a file.
#
#Tested on S_coelicolor whole genome from
#ftp://ftp.sanger.ac.uk/pub/S_coelicolor/whole_genome/
#
#There are two command line options:
#-i input file
#-o output file
#
#The input file is a single FASTA sequence record.
#
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk.

use warnings;
use strict;

#The pattern to match
my $pattern = "GT[GT][CG][CG][TG][GATCRYSWKMBDHVN]{12,16}A[TC][TG][CG]T";

#An array to hold results
my @resultsArray;

#Command line processing.
use Getopt::Long;

my $inputFile;
my $outputFile;

Getopt::Long::Configure ('bundling');
GetOptions ('i|input_file=s' => \$inputFile,
	    'o|output_file=s' => \$outputFile);

if(!defined($inputFile)) {
    die ("Usage: genome_search.pl -i <input file> -o <output file>\n");
}

if(!defined($outputFile)) {
    die ("Usage: genome_search.pl -i <input file> -o <output file>\n");
}

#Now open the file specified by $inputFile
open (INFILE, $inputFile) or die( "Cannot open file for input: $!" );
$/ = undef;
my $sequenceEntry = <INFILE>;

my $sequenceTitle;

#Match the sequence title portion of the sequence record. A sequence record looks like this:
#>my sequence title
#gattatatatatatttac
if ($sequenceEntry =~ m/^>([^\n\cM]+)/){
    $sequenceTitle = $1;
    $sequenceEntry =~ s/^>[^\n\cM]+//;
}
else {
    $sequenceTitle = "No_title_was_found";
}
$sequenceEntry =~ s/[^gatcryswkmbdhvn]//ig;
my $sequenceLength = length($sequenceEntry);

#Write header information to the file specified by $outputFile.
open(OUTFILE, ">" . $outputFile) or die ("Cannot open file for output: $!");
my $now = gmtime;
print(OUTFILE "#-------------------------------------------------------------------------------------------\n");
print(OUTFILE "#Results of genome_search.pl pattern search of file $inputFile performed at $now (GM time).\n");
print(OUTFILE "#Sequence title: $sequenceTitle, sequence length: $sequenceLength.\n");
print(OUTFILE "#Pattern used: $pattern\n");
close(OUTFILE) or die ("Cannot close file for output: $!");

my $strandSearched;
my $matchingSequence;
my $firstMatchingPosition;
my $lastMatchingPosition;

#Perform the search for the direct strand first.
print "Searching on the direct strand...\n";
$strandSearched = "direct";

while ($sequenceEntry =~ m/($pattern)/gi) {
    $matchingSequence = $1;
    $lastMatchingPosition = pos($sequenceEntry);
    $firstMatchingPosition = $lastMatchingPosition - length($matchingSequence) + 1;
    push( @resultsArray, "$matchingSequence\t$firstMatchingPosition\t$lastMatchingPosition\t$strandSearched\n");   
}#end of loop for each match


#Perform the search for the reverse strand.

#Do reverse complement and write to file
#complement the sequence
$sequenceEntry =~ tr/gatcryswkmbdhvnGATCRYSWKMBDHVN/ctagyrswmkvhdbnCTAGYRSWMKVHDBN/;

#reverse the sequence
$sequenceEntry = reverse($sequenceEntry);

print "Searching on the reverse strand...\n";
$strandSearched = "reverse";

while ($sequenceEntry =~ m/($pattern)/gi) {
    $matchingSequence = $1;
    #These calculations make the positions correspond to the direct strand.
    $firstMatchingPosition = $sequenceLength - pos($sequenceEntry) + 1;
    $lastMatchingPosition = $firstMatchingPosition + length($matchingSequence) - 1;
    push( @resultsArray, "$matchingSequence\t$firstMatchingPosition\t$lastMatchingPosition\t$strandSearched\n"); 
}#end of loop for each match

#Sort the results by start base using a map-sort-map.
@resultsArray = map { $_->[1] }
      sort { $a->[0] <=> $b->[0]}
      map { [ getStartBase($_), $_] }
      @resultsArray;

sub getStartBase {
    my $result = shift();
    $result =~ m/^[^\t]+\t(\d+)\t/;
    return $1;
}

my $numberOfMatches = scalar(@resultsArray);

open(OUTFILE, "+>>" . $outputFile) or die ("Cannot open file for output: $!");
print(OUTFILE "#There were $numberOfMatches matches to the pattern.\n");
print(OUTFILE "#Positions given correspond to the direct strand\n");
print(OUTFILE "#Each record in the output contains the following information:\n");
print(OUTFILE "#Match\tStart\tStop\tStrand\n");
print(OUTFILE "#-------------------------------------------------------------------------------------------\n\n");

foreach (@resultsArray) {
    print (OUTFILE $_);
}


close(OUTFILE) or die ("Cannot close file for output: $!");
