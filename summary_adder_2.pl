#!/usr/bin/perl
#summary_adder_2.pl
#Adds summary information to the output of the script blast_client.pl.
#The summary information is obtained remotely from NCBI. 
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk.

use warnings;
use strict;

#Perl comes with a group of modules that can be used to automate Web-related activities.
#We need to load two modules that we will be using.
use LWP::UserAgent;
use HTTP::Request::Common;

#Items sent to Entrez.
my $db = ""; #set to "Nucleotide" or to "Protein".
my $uid = ""; #unique identifier. 
my $url = "http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=text&db=$db&uid=$uid&dopt=DocSum";

#Create UserAgent object.
my $browser = LWP::UserAgent->new();
$browser->timeout(30);

#Check if file already exists.
if (-e "summary_added.txt") {
    die ("The output file summary_added.txt already exists. Please rename this file or move it to another directory.\n");
}

#Prompt the user for the name of the file to read.
print "This program creates a file called summary_added.txt.\n";
print "Cancel this program at any time by pressing Ctrl+C.\n";
print "Enter the name of the file to read and then press Enter:\n";
my $fileToRead = <STDIN>;
chomp($fileToRead);
open (INFILE, $fileToRead) or die( "Cannot open file : $!" );

#Determine the type of sequence hits to be processed.
print "Enter the type of search that was performed and then press Enter: (blastn, blastx, tblastx)\n";
my $searchType = <STDIN>;
if ($searchType =~ m/blastn/i) {
    $db = "Nucleotide";
}
elsif ($searchType =~ m/blastx|tblastx/i) {
    $db = "Protein";
}
else {
    die ("The search type was not understood.\n");
}

#Add some info to the output file.
open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
print (OUTFILE "#Summary information added to end of each entry using summary_adder.pl.\n");
close(OUTFILE) or die ("Cannot close file : $!");

#An entry index.
my $entryCount = 1;

#Read the file line by line.
while (my $line = <INFILE>) {
    if ($line =~ m/^[^\#][^\t]*\t([^\t]+)/m) {
	print "Reading entry $entryCount.\n";
	my $accessionEntry = $1;
	if ($accessionEntry =~ m/gi\|(\d+)\|/) {
	    $uid = $1;
	    my $response = $browser->request(POST ("http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=text&db=$db&uid=$uid&dopt=DocSum"));
	    if ($response->is_success()) {
		my $result = $response->as_string();
		if ($result =~ m/(<pre>[\s\S]+<\/pre>)/i) {
		    $result = $1;
		}
		else {
		    die ("The response received was not understood.\n");
		}

		my @resultArray = split(/\n+/, $result);

		#foreach (@resultArray) {
		  #  print "line is $_ \n";
		#}
		

		pop (@resultArray);
		pop (@resultArray);
		shift (@resultArray);
		#shift (@resultArray);


		print "Updating entry $entryCount.\n";
		my $outputString = join (" ", @resultArray);
		open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
		chomp($line);
		$line =~ s/\cM//;
		print (OUTFILE $line . "\t" . $outputString . "\n");
		close(OUTFILE) or die ("Cannot close file : $!");
	    }
	    else {
		die ("A response was not received for $accessionEntry.\n");
	    }
	}
	else {
	    print "$accessionEntry\n";
	    die ("There was a problem reading the gi for one of the entries.\n");
	}
	$entryCount = $entryCount + 1;
    }
    else { 
	open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
	$line =~ s/\cM//;
	print (OUTFILE "$line");
	close(OUTFILE) or die ("Cannot close file : $!");
	next;
    }

}#end of while loop.
close (INFILE) or die( "Cannot close file : $!");

print "summary_added.txt to view the results.\n";
