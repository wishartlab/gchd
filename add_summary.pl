#!/usr/bin/perl
#add_summary.pl
#Adds summary information to the last column of the Hit Table format
#produced by NCBI's BLAST program. When the BLAST formatting option
#"alignment view" is set to "Hit Table", the BLAST output is returned
#as a table, where each line consists of the identification number
#of the hit, followed by some similarity measures. This script uses the
#the identification number to retrieve a more detailed description of the
#hit sequence from NCBI.  This extra information is appended to the end
#of the existing entry, and then the new entry is written to a text file.
#Written by Paul Stothard, Genome Canada Bioinformatics Help Desk.

use warnings;
use strict;

#Perl comes with a group of modules that can be used to automate Web-related activities.
#We need to load two modules that we will be using.
use LWP::UserAgent;
use HTTP::Request::Common;

#Items sent to Entrez.
my $db = ""; #set to "Nucleotide" or to "Protein".
my $uid = ""; #unique identifier. 
my $url = "http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=text&db=$db&uid=$uid&dopt=DocSum";

#Create UserAgent object.
my $browser = LWP::UserAgent->new();
$browser->timeout(30);

#Check if file already exists.
if (-e "summary_added.txt") {
    die ("The output file summary_added.txt already exists. Please rename this file or move it to another directory.\n");
}

#Prompt the user for the name of the file to read.
print "Enter the name of the file to read and then press Enter:\n";
my $fileToRead = <STDIN>;
chomp($fileToRead);
open (INFILE, $fileToRead) or die( "Cannot open file : $!" );

#Add some info to the output file.
open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
print (OUTFILE "# Summary information added to end of each entry using add_summary.pl.\n");
close(OUTFILE) or die ("Cannot close file : $!");

#An entry index.
my $entryCount = 1;

#Read the file line by line.
while (my $line = <INFILE>) {
    if ($line =~ m/(\#\s[^\n]+)/) {
	$line = $1;
	if ($line =~ m/^\#\stblastn\s|^\#\sblastn\s|^\#\stblastx\s/i) {
	    $db = "Nucleotide";
	}
	if ($line =~ m/^\#\sblastp\s|^\#\sblastx\s/i) {
	    $db = "Protein";
	}
	if ($line =~ m/^\#\sfields:\s/i) {
	    $line = "# Fields: Query id, Subject id, % identity, alignment length, mismatches, gap openings, q. start, q. end, s. start, s. end, e-value, bit score, retrieved summary";
	}
	open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
	print(OUTFILE $line . "\n");
	close(OUTFILE) or die ("Cannot close file : $!");
	next;
    }
    if ($line =~ m/^\S+[\t]+(\S+)[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]+\S+[\t]*$/m) {
	if ($db eq "") {
	    #Determine the type of sequence hits to be processed.
	    print "Enter the type of search that was performed and then press Enter: (blastn, tblastn, tblastx, blastp, blastx)\n";
	    my $searchType = <STDIN>;
	    if ($searchType =~ m/tblastn|blastn|tblastx/i) {
		$db = "Nucleotide";
	    }
	    elsif ($searchType =~ m/blastx|blastp/i) {
		$db = "Protein";
	    }
	    else {
		die ("The search type was not understood.\n");
	    }
	}
	print "Reading entry $entryCount.\n";
	my $accessionEntry = $1;
	if ($accessionEntry =~ m/gi\|(\d+)\|/) {
	    $uid = $1;
	    my $response = $browser->request(POST ("http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?cmd=text&db=$db&uid=$uid&dopt=DocSum"));
	    if ($response->is_success()) {
		my $result = $response->as_string();
		if ($result =~ m/(<pre>[\s\S]+<\/pre>)/i) {
		    $result = $1;
		}
		else {
		    die ("The response received was not understood.\n");
		}
		
		#Process the result.
		$result =~ s/<pre>\s*|\s*<\/pre>//gi;
		$result =~ s/[^\n]+\n//;
		$result =~ s/[^\n]+$//;

		print "Updating entry $entryCount.\n";
		
		open(OUTFILE, "+>>summary_added.txt") or die ("Cannot open file : $!");
		chomp($line);
		print (OUTFILE $line . "\t" . $result . "\n");
		close(OUTFILE) or die ("Cannot close file : $!");
	    }
	    else {
		die ("A response was not received for $accessionEntry.\n");
	    }
	}
	else {
	    print "$accessionEntry\n";
	    die ("There was a problem reading the gi for one of the entries.\n");
	}
	$entryCount = $entryCount + 1;
    }
    else { 
	next;
    }

}#end of while loop.
close (INFILE) or die( "Cannot close file : $!");

print "Open summary_added.txt to view the results.\n";
